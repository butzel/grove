/**
 * Author: Butzel (opensource@butzel.info)
 * Version: <see VERSION_*>-Variable
 * License: GPLv3
 * 
 * This library enables the use of the module 'Grove - LCD RGB Backlight' from seeedstudio.
 * 
 * __NOTE__: You need an external 5 V supply, otherwise no text will be shown on the display
 */

//% color="#3B1" weight=5 icon="\u2603"
//% groups="['Backlight','Display']""
namespace Grove_LCD_RGB_Backlight {
    const VERSION_Grove_LCD_RGB_Backlight = 0; 

    /** 
     * Version of this library
     * License: GPLv3 butzel 
     * */
    //% block="Version"
    export function LCD_RGB_Backlight_Version(){
        return VERSION_Grove_LCD_RGB_Backlight;
    }

    // // // Display - AREA // // //
    /**
    * This block must be used once before using the LCD
    *
    */
    //% block="initialize LCD"
    //% group="Display"
    export function LCD_RGB_Backlight_INIT(){
        // wait for power rising up to 4.5V
        basic.pause(50);
        sendto_LCD_RGB_Backlight_CMD(0x28);   
        basic.pause(5); //wait again ;-)
        sendto_LCD_RGB_Backlight_CMD(0x28);
        basic.pause(2); //wait again ;-)
        sendto_LCD_RGB_Backlight_CMD(0x28);

        sendto_LCD_RGB_Backlight_CMD(0x0c); // change to 0x0c
        LCD_RGB_Backlight_ClearScreen(); 
        sendto_LCD_RGB_Backlight_CMD(0x06); // left to right  
    }

    /**
    * Clear the LCD 
    */
    //% block="Clear Screen"
    //% group="Display"
    export function LCD_RGB_Backlight_ClearScreen(){
        sendto_LCD_RGB_Backlight_CMD(0x01);
    }

    /**
    * move cursor to home position
    */
    //% block="Home"
    //% group="Display"
    export function LCD_RGB_Backlight_Home(){
        sendto_LCD_RGB_Backlight_CMD(0x02);
    }



    /**
    * hide cursor 
    */
    //% block="hide cursor"
    //% group="Display"
    //% advanced=true
    export function LCD_RGB_Backlight_NoCursor(){
        sendto_LCD_RGB_Backlight_CMD(0x0c);
    }

    /**
    *  Enable blinking cursor 
    */
    //% block="blinking cursor"
    //% group="Display"
    //% advanced=true
    export function LCD_RGB_Backlight_BlinkCursor(){
        sendto_LCD_RGB_Backlight_CMD(0x0d);
    }
    /**
    * show cursor as underscore
    */
    //% block="static cursor"
    //% group="Display"    
    //% advanced=true
    export function LCD_RGB_Backlight_UnderscoreCursor(){
        sendto_LCD_RGB_Backlight_CMD(0x0e);
    }

    /**
     *  Sets the position of the cursor 
     */
    //% block="set cursor to $column $line"
    //% group="Display"
    //% column.min=0 column.max=15 column.defl=0
    //% line.min=0 line.max=1 line.defl=0
    //% advanced=true
    export function LCD_RGB_Backlight_Cursor(column: number, line: number) {
        if(line == 0){
            column = column + 0x80;
        }else{
            column = column + 0xC0;
        }
        sendto_LCD_RGB_Backlight_CMD(column);
    }


    /**
     *  Write on the LCD 
     */
    //% block="Write on LCD $txt"
    //% group="Display"
    //% txt.defl="Calliope Mini"
    export function LCD_RGB_Backlight_Write(txt: string) {
        for(let i = 0; i < txt.length; i++){
            basic.pause(10);
            sendto_LCD_RGB_Backlight_DATA(txt.charCodeAt(i));
        }
    }
    /**
     * Write text on the LCD  
     */
    //% block="write on LCD $txt at $column, $line"
    //% group="Display"
    //% txt.defl="butzel"
    //% column.min=0 column.max=15 column.defl=5
    //% line.min=0 line.max=1 line.defl=1
    //% advanced=true
    export function LCD_RGB_Backlight_WriteAt(txt: string, column: number, line: number) {
        LCD_RGB_Backlight_Cursor(column,line);
        LCD_RGB_Backlight_Write(txt);
    }


    /**
     *  Display character at desired position
     */
    //% block="character $ascii at $column, $line"
    //% group="Display"
    //% ascii.defl=0 ascii.min=0 ascii.max=255
    //% column.min=0 column.max=15 column.defl=0
    //% line.min=0 line.max=1 line.defl=0
    //% advanced=true
    export function LCD_RGB_Backlight_CharAt(ascii: number, column: number, line: number) {
        LCD_RGB_Backlight_Cursor(column,line);
        sendto_LCD_RGB_Backlight_DATA(ascii);
    }

    // // // BACKLIGHT-AREA // // //

     /**
     *  Sets the backlight to white
     */
    //% block="turn Backlight on"let list: number[] = [1, 2, 3];
    //% group="Backlight"
    export function LCD_RGB_Backlight_backlightOn(){
        LCD_RGB_Backlight_backlightColor(255,255,255);
    }

    /**
     *  Sets the backlight by mixing the red, green, and blue values
     */
    //% block="set backlightColor to $red $green $blue"
    //% group="Backlight"
    //% red.min=0 red.max=255 red.defl=0
    //% green.min=0 green.max=255 green.defl=0
    //% blue.min=0 blue.max=255 blue.defl=0
    export function LCD_RGB_Backlight_backlightColor(red: number, green: number, blue: number) {
            init_LCD_RGB_Backlight_BL(); // uncool, aber vereinfacht es fuer die Kinder    
            red = red % 256;
            green = green % 256;
            blue = blue % 256; 
            sendto_LCD_RGB_Backlight_BL(4, red);  
            sendto_LCD_RGB_Backlight_BL(3, green);
            sendto_LCD_RGB_Backlight_BL(2, blue);
    }

    /**
     *  Sets the backlight off
     */
    //% block="turn backlight off"
    //% group="Backlight"
    export function LCD_RGB_Backlight_backlightOff(){
        LCD_RGB_Backlight_backlightColor(0,0,0);
    }
    
    /**
     *  Let the backlight blink
     */
    //% block="blinking backlight $speed"
    //% speed.min=0 speed.max=64 speed.defl=32
    //% group="Backlight"
    export function LCD_RGB_Backlight_backlight_Blink(speed: number) {
        speed = 64 - (speed%64);
        sendto_LCD_RGB_Backlight_BL(1, 32); //blinking enabled
        sendto_LCD_RGB_Backlight_BL(6,0x7f); // duty 50%
        sendto_LCD_RGB_Backlight_BL(7,speed);
    }
    
    /**
     *  stop backlight blinking
     */
    //% block="stop blinking backlight "
    //% group="Backlight"
    export function LCD_RGB_Backlight_backlight_StopBlink() {
        sendto_LCD_RGB_Backlight_BL(6,0xff); // duty 100%
        sendto_LCD_RGB_Backlight_BL(7,0);
        sendto_LCD_RGB_Backlight_BL(1, 0); //blinking disabled
    }




    // help functions 
    // send 16-Bit-Values to RGB-Backlight (I2C-Addr: 0x62)
    function sendto_LCD_RGB_Backlight_BL(addr: number,  value: number){
         value = (addr<<8) + value;
         pins.i2cWriteNumber(0x62, value, NumberFormat.UInt16BE);
    }
    // Initialize the RGB-Backlight
    function init_LCD_RGB_Backlight_BL() {
        sendto_LCD_RGB_Backlight_BL(0, 0);
        sendto_LCD_RGB_Backlight_BL(8, 255);
        sendto_LCD_RGB_Backlight_BL(1, 0); //blink
    }
    // send Data to LCD
    function sendto_LCD_RGB_Backlight_DATA(data: number){
        data = (0x40<<8) + data
        pins.i2cWriteNumber(0x3e, data, NumberFormat.UInt16BE)        

    }
    // send Data to LCD
    function sendto_LCD_RGB_Backlight_LCD(data: number[]){
        pins.i2cWriteBuffer(0x3e, Buffer.fromArray( data ));        
    }
     // send CMD to LCD
    function sendto_LCD_RGB_Backlight_CMD(cmd: number){
        cmd = (0x80<<8) + cmd
        pins.i2cWriteNumber(0x3e, cmd, NumberFormat.UInt16BE)        
    }

}
