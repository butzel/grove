#!/bin/bash
# this script needs npm
# GENTOO: emerge -qav net-libs/nodejs
OLDDIR=$(pwd)
mkdir -p ~/dev/PXT && cd ~/dev/PXT
npm install pxt
#ln -s ~/dev/PXT/node_modules/pxt/pxt ~/bin/pxt

cd $OLDDIR
~/dev/PXT/node_modules/pxt/pxt target calliope
~/dev/PXT/node_modules/pxt/pxt install 
 
~/dev/PXT/node_modules/pxt/pxt build

echo "add extension by local file:"
echo "   $OLDDIR/built/binary.hex"
 
